package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    HashMap<String, Double> bookQuotes = new HashMap<String,Double>();

    public Quoter(){
        bookQuotes.put("1",10.0);
        bookQuotes.put("2",45.0);
        bookQuotes.put("3",20.0);
        bookQuotes.put("4",35.0);
        bookQuotes.put("5",50.0);
        bookQuotes.put("others",0.0);
    }

    public double getBookPrice(String isbn){
        return bookQuotes.get(isbn);
    }
}
